﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointSet : MonoBehaviour
{
    // ---------------------------------------

    // Current index of waypoints
    int m_Index;

    // Size of waypoints list
    int m_Count;

    // List of all waypoints in this set
    List<Transform> m_WayPoints;

    // ---------------------------------------

    void Awake()
    {
        m_WayPoints = new List<Transform>();

        // Gather all waypoints
        foreach (Transform t in transform)
        {
            m_WayPoints.Add(t);
        }

        m_Count = m_WayPoints.Count;
        m_Index = 0;
    }

    public Transform NextPoint()
    {
        Transform next = m_WayPoints[m_Index];

        m_Index++;

        if (m_Index >= m_Count)
            m_Index = 0;

        return next;
    }

    // ---------------------------------------

    public List<Transform> WayPoints
    {
        get { return m_WayPoints; }
    }
}
