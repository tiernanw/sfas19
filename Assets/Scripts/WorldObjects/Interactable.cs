﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    [SerializeField]
    bool m_OneUse = true;

    [SerializeField]
    UIManager m_UIManager;

    bool m_Used = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || (m_OneUse && m_Used))
            return;

        m_UIManager.SetInteractState(true);
    }

    void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player") || (m_OneUse && m_Used))
            return;

        PlayerController player = other.GetComponent<PlayerController>();

        if (player.Input.Action)
        {
            Use(player);

            m_Used = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        m_UIManager.SetInteractState(false);
    }

    protected virtual void Use(PlayerController player)
    {

    }
}
