﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrapplePoint : MonoBehaviour
{
    // Used to stop player re-grabbing same point
    bool m_Used = false;

    void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        UIManager ui = other.GetComponent<PlayerController>().UI;

        ui.SetGrappleState(true);
    }

    void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        PlayerController playerController = other.GetComponent<PlayerController>();
        StateMachine<PlayerController> stateMachine = playerController.StateMachine;

        // Only want to grab when in air
        if (!stateMachine.CurrentState.Equals("airborne") || m_Used)
            return;

        if (playerController.Input.Grapple)
        {
            stateMachine.GoToState("swinging");
            stateMachine.SendMessage(this.gameObject);

            m_Used = true;

            playerController.UI.SetGrappleState(false);

            return;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (!other.CompareTag("Player"))
            return;

        m_Used = false;

        UIManager ui = other.GetComponent<PlayerController>().UI;

        ui.SetGrappleState(false);
    }
}
