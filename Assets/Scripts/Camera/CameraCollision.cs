﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollision : MonoBehaviour
{
    // ----------------------------

    [SerializeField]
    Transform m_Camera;

    [SerializeField]
    Transform m_Pivot;

    [SerializeField]
    float m_ZoomSpeed = 30f;

    [SerializeField]
    float m_RetreatSpeed = 10f;

    // ----------------------------

    // Radius of sphere cast should wrap this (less clipping)
    float m_CamNearPlane = 0.1f;

    // Intended distance from pivot
    float m_IntendedDistance = 4f;

    // Current camera offset
    Vector3 m_CamOffset = Vector3.forward * -4f;

    // ----------------------------

    void Start()
    {
        GetComponent<CameraFollow>().ModeChanged += CamModeChanged;
	}

	void LateUpdate()
    {
        RaycastHit hit;

        Vector3 targetPosition = m_CamOffset;
        float smoothSpeed = m_RetreatSpeed;

        // Layer Mask that hits everything except player
        int allButPlayer = ~(1 << 8);

        // Cast towards camera
        Vector3 directionToCast = (m_Camera.position - m_Pivot.position).normalized;

        // Sphere cast towards the camera to check for blockages
        if (Physics.SphereCast(m_Pivot.position, m_CamNearPlane, directionToCast, out hit, m_IntendedDistance, allButPlayer))
        {
            // Its known that if this is true it can mean there was an issue
            if (hit.distance == 0f)
                Physics.Raycast(m_Pivot.position, -m_Pivot.transform.forward, out hit, m_IntendedDistance, allButPlayer);

            float offset = (hit.point - m_Pivot.position).magnitude;
            targetPosition = -transform.forward * offset;
            smoothSpeed = m_ZoomSpeed;
        }

        m_Camera.localPosition = Vector3.Lerp(m_Camera.localPosition, targetPosition, Time.deltaTime * smoothSpeed);
    }

    void CamModeChanged(CameraMode mode, Vector3 camOffset)
    {
        m_IntendedDistance = camOffset.magnitude;
        m_CamOffset = camOffset;
    }
}
