﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void CameraModeChange(CameraMode mode, Vector3 camOffset);

public class CameraFollow : MonoBehaviour
{
    public event CameraModeChange ModeChanged;

    // -------------------------------------

    [Header("References")]

    // The Camera Target
    [SerializeField]
    Transform m_PlayerTransform;

    // Pivot creates better camera movement
    [SerializeField]
    Transform m_Pivot;

    [SerializeField]
    Camera m_Camera;

    [Header("Smoothing Values")]

    // Modifier for mouse input
    [SerializeField]
    float m_RotationSpeed = 120f;

    // Translation & rotation interpolation rates
    [SerializeField]
    float m_MoveSmoothing = 12.0f;

    [SerializeField]
    float m_RotationSmoothing = 18.0f;

    [Header("Limits")]

    // Max and minimum values the x rotation can be
    [SerializeField]
    float m_PitchMax = 80.0f;

    [SerializeField]
    float m_PitchMin = -45.0f;

    [Header("Positioning and Offsets")]

    // Location of the pivot in different modes
    [SerializeField]
    Vector3 m_AdventurePosition;

    [SerializeField]
    Vector3 m_CombatPosition;

    // -------------------------------------

    CameraMode m_CamMode = CameraMode.Adventure;

    // Camera's rotation around y
    float m_Yaw = 0f;

    // Camera's rotation around x
    float m_Pitch = 0f;

    // -------------------------------------

    void LateUpdate()
    {
        HandleMovement();
        HandleRotation();
    }

    private void HandleRotation()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        m_Yaw += mouseX * m_RotationSpeed * Time.deltaTime;
        m_Pitch -= mouseY * m_RotationSpeed * Time.deltaTime;

        m_Pitch = Mathf.Clamp(m_Pitch, m_PitchMin, m_PitchMax);

        Quaternion targetRotation = Quaternion.Euler(m_Pitch, m_Yaw, 0f);

        m_Pivot.rotation = m_RotationSmoothing != 0f
                    ? Quaternion.Slerp(m_Pivot.rotation, targetRotation, Time.deltaTime * m_RotationSmoothing)
                    : targetRotation;
    }

    private void HandleMovement()
    {
        transform.position = m_MoveSmoothing != 0f
                    ? Vector3.Lerp(transform.position, m_PlayerTransform.position, Time.deltaTime * m_MoveSmoothing)
                    : m_PlayerTransform.position;
    }

    // -------------------------------------

    public CameraMode Mode
    {
        get { return m_CamMode; }
        set
        {
            m_CamMode = value;

            Vector3 targetPos = value == CameraMode.Adventure
                        ? m_AdventurePosition
                        : m_CombatPosition;

            ModeChanged.Invoke(value, targetPos);
        }
    }

    public Vector3 AdventurePosition
    {
        get { return m_AdventurePosition; }
    }

    public Vector3 CombatPosition
    {
        get { return m_CombatPosition; }
    }

    public Camera Cam
    {
        get { return m_Camera; }
    }
}

public enum CameraMode
{
    Adventure,
    Combat
}
