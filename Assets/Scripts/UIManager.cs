﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    // --------------------------------------------------------------

    [SerializeField]
    Health m_PlayerHealth;

    [Header("Text References")]

    [SerializeField]
    Text m_BulletText;

    [SerializeField]
    Text m_GrenadeText;

    [SerializeField]
    Text m_HealthText;

    [Header("Others")]

    [SerializeField]
    Image m_Reticle;

    [SerializeField]
    GameObject m_InteractNotify;

    [SerializeField]
    GameObject m_GrappleNotify;

    // --------------------------------------------------------------

    void Start()
    {
        Cursor.visible = false;
    }

    void OnEnable()
    {
        m_PlayerHealth.OnHealthChanged += UpdateHealth;
    }

    void OnDisable()
    {
        m_PlayerHealth.OnHealthChanged -= UpdateHealth;
        Cursor.visible = true;
    }

    void UpdateHealth(int amount)
    {
        m_HealthText.text = "Health: " + amount;
    }

    public void SetAmmoText(int bulletCount, int grenadeCount)
    {
        if(m_BulletText)
        {
            m_BulletText.text = "Bullets: " + bulletCount;
        }
        
        if(m_GrenadeText)
        {
            m_GrenadeText.text = "Grenades: " + grenadeCount;
        }
    }

    public void SetAmmoTextState(bool state)
    {
        m_BulletText.enabled = state;
        m_GrenadeText.enabled = state;
    }

    public void SetReticleState(bool state)
    {
        m_Reticle.enabled = state;
    }

    public void SetInteractState(bool state)
    {
        m_InteractNotify.SetActive(state);
    }

    public void SetGrappleState(bool state)
    {
        m_GrappleNotify.SetActive(state);
    }
}
