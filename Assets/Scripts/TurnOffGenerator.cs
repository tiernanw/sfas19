﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;


public class TurnOffGenerator : Interactable
{
    [SerializeField]
    PlayableDirector m_Playable;

    [SerializeField]
    AudioSource m_AudioSource;

    [SerializeField]
    AudioClip m_NewMusic;

    protected override void Use(PlayerController player)
    {
        m_Playable.Play();

        m_AudioSource.clip = m_NewMusic;

        m_AudioSource.PlayDelayed(1f);
    }
}
