﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootIK : MonoBehaviour
{
    [SerializeField]
    float m_FootOffset = 0.1f;

    PlayerController m_Player;

    Animator m_Anim;

    Vector3 m_BodyPosition;

    Vector3 rFootPosition;
    Vector3 lFootPosition;

    Quaternion rFootRotation;
    Quaternion lFootRotation;

	void Start()
    {
        m_Player = GetComponent<PlayerController>();
        m_Anim = GetComponent<Animator>();
	}
	
	void Update()
    {
		
	}

    void OnAnimatorIK(int layerIndex)
    {
        float leftWeight = m_Anim.GetFloat("LeftFoot");
        float rightWeight = m_Anim.GetFloat("RightFoot");

        CorrectFootPosition(HumanBodyBones.LeftFoot, ref lFootPosition, ref lFootRotation);
        CorrectFootPosition(HumanBodyBones.RightFoot, ref rFootPosition, ref rFootRotation);

        SetFootPosition(AvatarIKGoal.LeftFoot, leftWeight, lFootPosition, lFootRotation);
        SetFootPosition(AvatarIKGoal.RightFoot, rightWeight, rFootPosition, rFootRotation);

        //AdjustBodyHeight();
    }

    void AdjustBodyHeight()
    {
        float rDiff = transform.position.y - rFootPosition.y;
        float lDiff = transform.position.y - lFootPosition.y;

        float greatest = Mathf.Max(rDiff, lDiff);

        Vector3 targetPosition = m_Anim.bodyPosition + (Vector3.down * greatest);
        m_BodyPosition = Vector3.Lerp(m_BodyPosition, targetPosition, Time.deltaTime * 10f);

        m_Anim.bodyPosition = m_BodyPosition;
    }

    void CorrectFootPosition(HumanBodyBones foot, ref Vector3 position, ref Quaternion rotation)
    {
        Transform footT = m_Anim.GetBoneTransform(foot);

        RaycastHit hit;
        if (Physics.Raycast(footT.position, Vector3.down, out hit, 1.5f))
        {
            position = hit.point;
            rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
        }
    }

    void SetFootPosition(AvatarIKGoal IKGoal, float weight, Vector3 position, Quaternion rotation)
    {
        m_Anim.SetIKPosition(IKGoal, position + Vector3.up * m_FootOffset);
        m_Anim.SetIKPositionWeight(IKGoal, weight);
        m_Anim.SetIKRotation(IKGoal, rotation * transform.rotation);
        m_Anim.SetIKRotationWeight(IKGoal, weight);
    }
}
