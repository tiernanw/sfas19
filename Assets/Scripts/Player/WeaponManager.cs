﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    [SerializeField]
    GunLogic gun;

    public void FireAt(Vector3 atPoint)
    {
        gun.Fire(atPoint);
    }

    public void SecondaryFireAt(Vector3 atPoint)
    {
        gun.FireGrenade(atPoint);
    }
}
