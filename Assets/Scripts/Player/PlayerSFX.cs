﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSFX : MonoBehaviour
{
    [SerializeField]
    AudioClip[] m_FeetSounds;

    [SerializeField]
    AudioClip[] m_JumpSounds;

    AudioSource m_Source;

    void Start()
    {
        m_Source = GetComponent<AudioSource>();
    }

    public void PlayFootSound()
    {
        PlayRandomSound(m_FeetSounds);
    }

    public void PlayJumpSound()
    {
        PlayRandomSound(m_JumpSounds);
    }

    void PlayRandomSound(AudioClip[] arr)
    {
        int index = Random.Range(0, arr.Length - 1);

        m_Source.PlayOneShot(arr[index]);
    }
}
