﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(InputManager))]
public class PlayerController : MonoBehaviour
{
    #region Public Properties

    public InputManager Input
    {
        get { return m_Input; }
    }

    public bool IsAlive
    {
        get { return m_IsAlive; }
    }

    public bool IsGrounded
    {
        get { return m_IsGrounded; }
    }

    public bool GroundedOnSteps
    {
        get { return m_GroundedOnSteps; }
        set { m_GroundedOnSteps = value; }
    }

    public bool UseGravity
    {
        get { return m_UseGravity; }
        set { m_UseGravity = value; }
    }

    public bool UseRootMotion
    {
        get { return m_UseRootMotion; }
        set { m_UseRootMotion = value; }
    }

    public float Gravity
    {
        get { return m_Gravity; }
    }

    public float RunSpeed
    {
        get { return m_RunSpeed; }
    }

    public float WalkSpeed
    {
        get { return m_WalkSpeed; }
    }

    public float SwingSpeed
    {
        get { return m_SwingSpeed; }
    }

    public float MaxSwingAngle
    {
        get { return m_MaxSwingAngle; }
    }

    public float SwingAngleBoost
    {
        get { return m_SwingAngleBoost; }
    }

    public float TetherMaxLength
    {
        get { return m_TetherMaxLength; }
    }

    public float TetherMinLength
    {
        get { return m_TetherMinLength; }
    }

    public StateMachine<PlayerController> StateMachine
    {
        get { return m_StateMachine; }
    }

    public WeaponManager Weapons
    {
        get { return m_WeaponManager; }
    }

    public Animator Anim
    {
        get { return m_Anim; }
    }

    public Vector3 StickInput
    {
        get { return m_StickInput; }
    }

    public Vector3 WaistCorrection
    {
        get { return m_WaistTargetDir; }
        set { m_WaistTargetDir = value; }
    }

    public CameraFollow CameraController
    {
        get { return m_CamController; }
    }

    public UIManager UI
    {
        get { return m_UIManager; }
    }

    public Transform GrappleHolder
    {
        get { return m_GrappleHolder; }
    }

    public Transform MidRightHand
    {
        get { return m_MidRightHand; }
    }

    public Transform MidLeftHand
    {
        get { return m_MidLeftHand; }
    }

    public Transform CameraTransform
    {
        get { return m_Camera; }
    }

    #endregion

    #region Serialized Fields

    [Header("Movement Settings")]

    // The character's walk speed
    [SerializeField]
    float m_WalkSpeed = 1.607f;

    // The character's running speed
    [SerializeField]
    float m_RunSpeed = 5.0f;

    // How quickly to adjust when speed changes
    [SerializeField]
    float m_SpeedChangeRate = 8.0f;

    // The gravity strength
    [SerializeField]
    float m_Gravity = 60.0f;

    // The maximum speed the character can fall
    [SerializeField]
    float m_MaxFallSpeed = 20.0f;

    // The character's jump height
    [SerializeField]
    float m_JumpHeight = 4.0f;

    // Amount of velocity to add when jumping forward
    [SerializeField]
    float m_JumpZBoost = 0.6f;

    [Header("Swing Settings")]

    // How quickly the player swings on ropes
    [SerializeField]
    float m_SwingSpeed = 2f;

    // Maximum angle the player can swing to at full momentum
    [SerializeField]
    float m_MaxSwingAngle = 60f;

    // Rate angle gets incremented each cycle
    [SerializeField]
    float m_SwingAngleBoost = 20f;

    // Grapple rope minimum and maximum lengths
    [SerializeField]
    float m_TetherMaxLength = 8f;

    [SerializeField]
    float m_TetherMinLength = 2f;

    [Header("References")]

    [SerializeField]
    CameraFollow m_CamController;

    [SerializeField]
    UIManager m_UIManager;

    // Object used to allow the player's mesh to rotate in the middle
    [SerializeField]
    Transform m_PlayerCenterRotater;

    // The point at which the grapple is connected to body
    [SerializeField]
    Transform m_GrappleHolder;

    // Middle of right and left hand
    [SerializeField]
    Transform m_MidRightHand;

    [SerializeField]
    Transform m_MidLeftHand;

    #endregion

    #region Private Fields

    bool m_IsGrounded = true;

    // If player is grounded when ground is as far away as step limit
    bool m_GroundedOnSteps = true;

    GroundInfo m_GroundInfo = new GroundInfo();

    bool m_UseGravity = true;

    // Used for giving weight and realism to animations
    bool m_UseRootMotion = true;

    InputManager m_Input;

    CharacterController m_CharacterController;

    Transform m_Camera;

    Animator m_Anim;

    // State machine to update the player's state
    StateMachine<PlayerController> m_StateMachine;

    // Manages all weapons for better communication
    WeaponManager m_WeaponManager;

    // Input of stick/WASD this frame
    Vector3 m_StickInput = Vector3.zero;

    // Raw input (useful for interpolation)
    Vector3 m_LocalMovementDir = Vector3.zero;

    // The current movement direction in x & z.
    Vector3 m_MovementDirection = Vector3.zero;

    // The current movement speed
    float m_MovementSpeed = 0.0f;

    // The current vertical / falling speed
    float m_VerticalSpeed = 0.0f;

    // Y velocity of jump
    float m_JumpYSpeed = 0.0f;

    // The current movement offset
    Vector3 m_CurrentMovementOffset = Vector3.zero;

    // The starting position of the player
    Vector3 m_SpawningPosition = Vector3.zero;

    // Whether the player is alive or not
    bool m_IsAlive = true;

    // The time it takes to respawn
    const float MAX_RESPAWN_TIME = 3.0f;
    float m_RespawnTime = MAX_RESPAWN_TIME;

    // The force added to the player (used for knockbacks)
    Vector3 m_Force = Vector3.zero;

    // Zero means don't correct waist, this is for my lack of animations, see late update
    Vector3 m_WaistTargetDir = Vector3.zero;

    #endregion

    #region Private Methods

    void Awake()
    {
        m_Input = GetComponent<InputManager>();
        m_CharacterController = GetComponent<CharacterController>();
        m_Anim = GetComponent<Animator>();
        m_WeaponManager = GetComponent<WeaponManager>();

        m_JumpYSpeed = Mathf.Sqrt(2f * m_JumpHeight * m_Gravity);
    }

    void Start()
    {
        m_SpawningPosition = transform.position;
        m_Camera = m_CamController.Cam.transform;

        m_UIManager.SetAmmoTextState(false);
        m_UIManager.SetReticleState(false);

        m_StateMachine = new StateMachine<PlayerController>(this);

        m_StateMachine.AddState("locomotion", new Locomotion());
        m_StateMachine.AddState("climbing", new Climbing());
        m_StateMachine.AddState("combat", new Combat());
        m_StateMachine.AddState("jumping", new Jumping());
        m_StateMachine.AddState("airborne", new AirBorne());
        m_StateMachine.AddState("swinging", new Swinging());
        m_StateMachine.AddState("dead", new Dead());

        m_StateMachine.GoToState("locomotion");
    }

    void OnEnable()
    {
        GetComponent<Health>().OnUnitDead += Die;
    }

    void OnDisable()
    {
        GetComponent<Health>().OnUnitDead -= Die;
    }

    void Update()
    {
        // If the player is dead update the respawn timer and exit update loop
        if (!m_IsAlive)
        {
            UpdateRespawnTime();
            return;
        }

        CheckForGround();

        m_Input.UpdateMoveInput();

        ApplyGravity();

        m_StateMachine.Update();

        UpdateMovementState();

        SlideOffSlopes(ref m_CurrentMovementOffset);

        m_GroundInfo.Reset();

        if (m_CharacterController.enabled)
            m_CharacterController.Move(m_CurrentMovementOffset * Time.deltaTime);
    }

    void LateUpdate()
    {
        m_StateMachine.LateUpdate();

        // Means we dont want to target anything
        if (m_WaistTargetDir == Vector3.zero)
            return;

        // Only need this because I don't have a correctly rotated anim for aiming 
        Transform spineBone = m_Anim.GetBoneTransform(HumanBodyBones.Spine);

        Transform rightHand = m_Anim.GetBoneTransform(HumanBodyBones.RightHand);

        // Aim up or down
        Vector3 direction = (m_WaistTargetDir - rightHand.position).normalized;

        Quaternion lookAt = Quaternion.LookRotation(direction);

        // Apply aim direction then correct yaw
        spineBone.rotation = lookAt * Quaternion.Euler(0, 10f, 0);

        // Correct pitch
        spineBone.localRotation *= Quaternion.Euler(12f, 0f, 0f);
    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        m_StateMachine.SendMessage(hit);

        m_GroundInfo.Inspect(hit);
    }

    void CheckForGround()
    {
        if (m_GroundedOnSteps)
        {
            Vector3 castFrom = transform.position + Vector3.up * (m_CharacterController.radius + m_CharacterController.skinWidth);
            float distanceToCast = m_CharacterController.radius + m_CharacterController.skinWidth + m_CharacterController.stepOffset;

            // Having this allows player to walk off steps if they are on an edge
            RaycastHit hit;
            if (Physics.Raycast(castFrom, Vector3.down, out hit, distanceToCast))
            {
                m_GroundInfo.Inspect(hit.normal, hit.collider.tag);
            }
        }

        // Allows character slide off slopes
        if (m_GroundInfo.Angle > m_CharacterController.slopeLimit)
            m_IsGrounded = false;
        else
            m_IsGrounded = m_CharacterController.isGrounded;

        m_Anim.SetBool("isGrounded", m_IsGrounded);
    }

    void ApplyGravity()
    {
        if (!m_UseGravity)
            return;

        // Stops player zooming down off a ledge
        if (m_IsGrounded)
        {
            m_VerticalSpeed = -m_Gravity;
            return;
        }

        // Apply gravity
        m_VerticalSpeed -= m_Gravity * Time.deltaTime;

        // Make sure we don't fall any faster than m_MaxFallSpeed.
        m_VerticalSpeed = Mathf.Max(m_VerticalSpeed, -m_MaxFallSpeed);
        m_VerticalSpeed = Mathf.Min(m_VerticalSpeed, m_MaxFallSpeed);
    }

    void UpdateMovementState()
    {
        m_CurrentMovementOffset = ((m_UseRootMotion ? m_Anim.velocity : m_MovementDirection)
                    + m_Force + new Vector3(0, m_VerticalSpeed, 0));

        m_Force *= 0.95f;

        m_Anim.SetFloat("SpeedXZ", m_MovementDirection.magnitude);

        m_Anim.SetFloat("Speed", m_CurrentMovementOffset.magnitude);
    }

    void SlideOffSlopes(ref Vector3 velocity)
    {
        if (m_GroundInfo.Angle > m_CharacterController.slopeLimit && m_VerticalSpeed < 0f)
        {
            velocity = Vector3.ProjectOnPlane(velocity, m_GroundInfo.Normal);
        }
    }

    void RotateCharacter(Vector3 movementDirection)
    {
        Quaternion lookRotation = Quaternion.LookRotation(movementDirection);
        if (transform.rotation != lookRotation)
        {
            transform.rotation = lookRotation;
        }
    }

    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void UpdateRespawnTime()
    {
        m_RespawnTime -= Time.deltaTime;
        if (m_RespawnTime < 0.0f)
        {
            Respawn();
        }
    }

    void Respawn()
    {
        m_IsAlive = true;
        m_Anim.enabled = true;
        transform.position = m_SpawningPosition;
        transform.rotation = Quaternion.identity;

        GetComponent<Health>().RestoreAll();

        m_StateMachine.GoToState("locomotion");
    }

    #endregion

    #region Public Methods

    public void Jump()
    {
        m_VerticalSpeed = m_JumpYSpeed;
        m_MovementDirection += transform.forward * m_JumpZBoost;
    }

    public void ResetVerticalSpeed()
    {
        m_VerticalSpeed = 0f;
    }

    public void DirectMove(Vector3 moveVector)
    {
        m_CharacterController.Move(moveVector);
    }

    public void SetVelocity(Vector3 velocity)
    {
        m_MovementDirection.Set(velocity.x, 0f, velocity.z);
        m_VerticalSpeed = velocity.y;
    }

    public void StopMoving()
    {
        m_LocalMovementDir = Vector3.zero;
        m_MovementDirection = Vector3.zero;
        m_VerticalSpeed = 0f;

        m_Anim.SetFloat("Forward", 0f);
    }

    public void MoveXZPlane(bool isStrafing = false)
    {
        float moveSpeed = !m_Input.WalkHeld ? m_RunSpeed : m_WalkSpeed;

        m_MovementSpeed = Mathf.MoveTowards(m_MovementSpeed, moveSpeed, Time.deltaTime * m_SpeedChangeRate);

        Vector3 inputVector = m_Input.RawMoveInput;

        if (inputVector.magnitude > 1f)  // Stops player moving too fast
            inputVector.Normalize();

        inputVector *= m_MovementSpeed;

        if (inputVector.sqrMagnitude != 0f && m_LocalMovementDir == Vector3.zero)
            m_LocalMovementDir = Quaternion.Euler(0f, transform.eulerAngles.y - m_Camera.eulerAngles.y, 0f) * Vector3.forward * 0.1f;

        m_LocalMovementDir = Vector3.Slerp(m_LocalMovementDir, inputVector, Time.deltaTime * m_SpeedChangeRate);

        m_MovementDirection = Quaternion.Euler(0f, m_Camera.eulerAngles.y, 0f) * m_LocalMovementDir;

        m_Anim.SetFloat("Forward", !isStrafing ? m_MovementDirection.magnitude : m_LocalMovementDir.z);
        m_Anim.SetFloat("Right", !isStrafing ? 0f : m_LocalMovementDir.x);
    }

    public void RotateToMoveDirection()
    {
        RotateCharacter(m_MovementDirection);
    }

    public void RotateToCamera(bool xzOnly)
    {
        Vector3 camForward = m_Camera.forward;
        
        if (xzOnly)
        {
            camForward.y = 0f;
            camForward.Normalize();
        }

        RotateCharacter(camForward);
    }

    public void RotateMeshTo(Vector3 direction)
    {
        m_PlayerCenterRotater.rotation = Quaternion.LookRotation(direction);
    }

    public void Die()
    {
        // Stops timer constantly resetting
        if (!m_IsAlive)
            return;

        m_IsAlive = false;
        m_RespawnTime = MAX_RESPAWN_TIME;
        m_Anim.enabled = false;

        m_StateMachine.GoToState("dead");
    }

    public void SetCollisionsState(bool state)
    {
        m_CharacterController.enabled = state;
    }

    public void AddForce(Vector3 force)
    {
        m_Force += force;
    }

    #endregion

    class GroundInfo
    {
        public bool Found { get; private set; }
        public Vector3 Normal { get; private set; }
        public float Angle { get; private set; }
        public string Tag { get; private set; }

        public void Inspect(ControllerColliderHit hit)
        {
            Inspect(hit.normal, hit.collider.tag);
        }

        public void Inspect(Vector3 normal, string tag)
        {
            if (normal.y < 0f) // definitely not ground
                return;

            // Check if ground has less slope than previous
            if (normal.y > Normal.y)
            {
                Found = true;
                Normal = normal;
                Angle = Vector3.Angle(Vector3.up, normal);
                Tag = tag;
            }
        }

        public void Reset()
        {
            Found = false;
            Normal = Vector3.zero;
            Angle = 0f;
            Tag = "";
        }
    }
}
