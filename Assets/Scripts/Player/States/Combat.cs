﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combat : StateBase<PlayerController>
{
    Vector3 m_AimingAt = Vector3.zero;

    public override void OnEnter(PlayerController obj)
    {
        obj.Anim.SetBool("isPistol", true);

        obj.CameraController.Mode = CameraMode.Combat;

        obj.UI.SetReticleState(true);
        obj.UI.SetAmmoTextState(true);
    }

    public override void OnExit(PlayerController obj)
    {
        obj.Anim.SetBool("isPistol", false);

        obj.CameraController.Mode = CameraMode.Adventure;

        obj.UI.SetReticleState(false);
        obj.UI.SetAmmoTextState(false);

        obj.WaistCorrection = Vector3.zero;
    }

    public override void Update(PlayerController obj)
    {
        if (!obj.Input.CombatHeld)
        {
            obj.StateMachine.GoToState("locomotion");
            return;
        }
        else if (!obj.IsGrounded)
        {
            obj.ResetVerticalSpeed();
            obj.StateMachine.GoToState("airborne");
            return;
        }

        CalculateAimDirection(obj);

        if (Input.GetMouseButtonDown(0))
        {
            obj.Weapons.FireAt(m_AimingAt);
        }
        else if (Input.GetMouseButtonDown(2))
        {
            obj.Weapons.SecondaryFireAt(m_AimingAt);
        }

        obj.MoveXZPlane(true);

        obj.RotateToCamera(true);
    }

    private void CalculateAimDirection(PlayerController obj)
    {
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));

        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, Mathf.Infinity, ~(1 << 8)))
        {
            m_AimingAt = hit.point;
            obj.WaistCorrection = hit.point;
        }
        else
        {
            m_AimingAt = Camera.main.transform.position + Camera.main.transform.forward * 100f;
            obj.WaistCorrection = Camera.main.transform.position + Camera.main.transform.forward * 100f;
        }
    }
}
