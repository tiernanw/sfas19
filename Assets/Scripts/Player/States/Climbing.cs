﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Climbing : StateBase<PlayerController>
{
    bool m_IsClimbingUp = false;

    Vector3 m_TargetPos;

    public override void OnEnter(PlayerController obj)
    {
        m_IsClimbingUp = false;

        obj.SetVelocity(Vector3.zero);

        obj.UseGravity = false;

        obj.UseRootMotion = true;
    }

    public override void OnExit(PlayerController obj)
    {
        obj.UseGravity = true;

        obj.UseRootMotion = false;
    }

    public override void Update(PlayerController obj)
    {
        AnimatorStateInfo animState = obj.Anim.GetCurrentAnimatorStateInfo(0);
       
        // Transitioning into locomotion
        if (m_IsClimbingUp)
        {
            if (animState.IsName("Crouched To Standing"))
            {
                obj.Anim.applyRootMotion = false;
                obj.SetCollisionsState(true);

                obj.StateMachine.GoToState("locomotion");
                return;
            }  
        }

        if (obj.Input.Jump)
        {
            // Difference in root position for climb up anim
            obj.transform.position += Vector3.up * 0.36f;

            obj.Anim.SetTrigger("ClimbUp");

            obj.Anim.applyRootMotion = true;
            obj.SetCollisionsState(false);

            obj.StopMoving();

            m_IsClimbingUp = true;
        }
    }
}
