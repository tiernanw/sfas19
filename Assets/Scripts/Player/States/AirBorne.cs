﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirBorne : StateBase<PlayerController>
{
    bool m_EnteringClimb = false;

    public override void OnEnter(PlayerController obj)
    {
        m_EnteringClimb = false;
    }

    public override void OnExit(PlayerController obj)
    {
        
    }

    public override void Update(PlayerController obj)
    {
        if (m_EnteringClimb)
        {
            AnimatorStateInfo animState = obj.Anim.GetCurrentAnimatorStateInfo(0);

            if (animState.IsName("Braced Hanging Idle"))
                obj.StateMachine.GoToState("climbing");

            return;
        }

        if (obj.IsGrounded)
        {
            obj.StateMachine.GoToState("locomotion");
            return;
        }

        CheckForLedges(obj);
    }

    private void CheckForLedges(PlayerController obj)
    {
        RaycastHit hHit, vHit;
        if (Physics.Raycast(obj.transform.position + Vector3.up * 1.75f, obj.transform.forward, out hHit, 0.4f))
        {
            if (!hHit.collider.CompareTag("Climbable"))
                return;

            if (Physics.Raycast(hHit.point + Vector3.up * 0.1f + obj.transform.forward * 0.1f, Vector3.down, out vHit, 0.2f))
            {
                Vector3 ledgePoint = new Vector3(hHit.point.x, vHit.point.y, hHit.point.z);

                Vector3 targetPosition = ledgePoint + hHit.normal * 0.3f + Vector3.down * 1.75f;

                obj.StopMoving();

                obj.transform.position = targetPosition;
                obj.transform.rotation = Quaternion.LookRotation(-hHit.normal);

                m_EnteringClimb = true;

                obj.UseGravity = false;

                obj.Anim.SetTrigger("GrabLedge");
            }
        }
    }
}
