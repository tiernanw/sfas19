﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : StateBase<PlayerController>
{
    public override void OnEnter(PlayerController obj)
    {
        obj.Anim.SetBool("isJumping", true);
    }

    public override void OnExit(PlayerController obj)
    {
        obj.Anim.SetBool("isJumping", false);
    }

    public override void Update(PlayerController obj)
    {
        AnimatorStateInfo animState = obj.Anim.GetCurrentAnimatorStateInfo(0);

        // Allows for transition animations
        if (!animState.IsName("RJumpAir"))
            return;

        obj.Jump();
        obj.StateMachine.GoToState("airborne");
    }
}
