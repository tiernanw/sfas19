﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Locomotion : StateBase<PlayerController>
{
    public override void OnEnter(PlayerController obj)
    {
        obj.UseRootMotion = true;

        obj.GroundedOnSteps = true;
    }

    public override void OnExit(PlayerController obj)
    {
        obj.UseRootMotion = false;

        obj.GroundedOnSteps = false;
    }

    public override void Update(PlayerController obj)
    {
        if (!obj.IsGrounded)
        {
            obj.ResetVerticalSpeed();
            obj.StateMachine.GoToState("airborne");
            return;
        }
        else if (obj.Input.CombatHeld)
        {
            obj.StateMachine.GoToState("combat");
            return;
        }
        else if (obj.Input.Jump)
        {
            obj.StateMachine.GoToState("jumping");
            return;
        }

        obj.MoveXZPlane();

        if (obj.Input.RawMoveInput.sqrMagnitude != 0f)
            obj.RotateToMoveDirection();
    }
}
