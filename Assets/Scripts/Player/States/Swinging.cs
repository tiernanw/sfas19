﻿using System.Collections; 
using System.Collections.Generic;
using UnityEngine;

public class Swinging : StateBase<PlayerController>
{
    // Used to render grapple line
    LineRenderer m_LineRenderer;

    // Point that player is attached to
    Vector3 m_TetherPoint = new Vector3(0f, 10f, 6f);

    // Point directly below by tether length for reference
    Vector3 m_ReferencePoint = new Vector3(0f, 4f, 6f);

    // Length of rope (specifically character to tether point)
    float m_TetherLength = 6f;

    // Current angle player is actually at
    float m_AngleForward = 0f;

    // Current speed player is at
    float m_SpeedForward = 0f;

    // Current angle player is left,right
    float m_AngleRight = 0f;

    // Current angle player can swing to
    float m_ForwardSwingAngle = 0f;

    // Current angle players swings to left and right
    float m_RightSwingAngle = 0f;

    // Angle player is targetting to swing to
    float m_TargetSwingAngle = 0f;
    
    // Time started swinging
    float m_StartTime = Time.time;

    // Time of swing player is currently at
    float m_Time = Time.time;

    bool m_IsSwinging = false;

    // Player is adjusting length of tether
    bool m_AdjustingLength = false;

    // Player's speed of swinging can increase
    bool m_CanBoost = false;

    // Offset used in cos/sin to offset movement
    float m_TrigOffset = 0f;

    // ----------------------------------------------------------------

    public Swinging()
    {
        GameObject LineRenderObj = new GameObject("Rope Renderer");
        LineRenderObj.AddComponent<LineRenderer>();

        m_LineRenderer = LineRenderObj.GetComponent<LineRenderer>();
        m_LineRenderer.enabled = false;

        m_LineRenderer.material.color = Color.black;
    }

    public override void OnEnter(PlayerController obj)
    {
        obj.Anim.SetBool("isRope", true);

        obj.Anim.SetTrigger("Grab");

        m_LineRenderer.enabled = true;

        obj.UseGravity = false;
    }

    public override void OnExit(PlayerController obj)
    {
        obj.Anim.SetBool("isRope", false);

        obj.Anim.SetBool("isSwinging", false);

        obj.UseGravity = true;

        m_LineRenderer.enabled = false;

        obj.RotateMeshTo(obj.transform.forward);
    }

    public override void Update(PlayerController obj)
    {
        if (obj.IsGrounded) // hit ground while swinging
        {
            obj.StateMachine.GoToState("locomotion");
            return;
        }
        else if (obj.Input.LetGo)
        {
            obj.StopMoving();
            obj.StateMachine.GoToState("airborne");
            obj.Anim.SetTrigger("LetGo");
            return;
        }
        else if (obj.Input.Jump)
        {
            obj.Jump();
            obj.StateMachine.GoToState("airborne");
            obj.Anim.SetTrigger("RopeJump");
            return;
        }

        HandleLengthAdjustment(obj);

        bool wasSwinging = m_IsSwinging;

        m_IsSwinging = obj.Input.RawMoveInput.z > 0.1f && !m_AdjustingLength;

        obj.Anim.SetBool("isSwinging", m_IsSwinging);

        if (m_IsSwinging && !wasSwinging && m_ForwardSwingAngle == 0f)
        {
            m_TargetSwingAngle = 10f;  // Player wont move unless nudged
        }

        HandleMovement(obj);

        HandleRotation(obj);
    }

    public override void LateUpdate(PlayerController obj)
    {
        Vector3 directionToTether = (m_TetherPoint - obj.transform.position).normalized;

        // Draw the player back towards the tether point if they have deviated
        obj.transform.position = m_TetherPoint - directionToTether * m_TetherLength;

        DrawRope(obj);
    }

    public override void HandleMessage(PlayerController obj, object o)
    {
        if (o is ControllerColliderHit)
        {
            ControllerColliderHit hit = (ControllerColliderHit)o;

            Debug.Log("Colliding with: " + hit.collider.name);

            HandleCollision(obj, hit);
        }
        else if (o is GameObject)
        {
            GameObject externalObject = (GameObject)o;

            if (!externalObject.CompareTag("GrapplePoint"))
                return;

            SetUpFromGrapplePoint(obj, externalObject);
        }
    }

    void HandleCollision(PlayerController obj, ControllerColliderHit hit)
    {
        m_ForwardSwingAngle = m_TargetSwingAngle = Mathf.Abs(AngleOfPlayerToTether(obj, obj.transform.forward));

        m_StartTime = Time.time;

        float dot = Vector3.Dot(obj.transform.forward, hit.normal);

        // Where we reset to depends on which direction we hit (front or back)
        m_TrigOffset = dot < 0f ? 0.5f * Mathf.PI : (3f / 2) * Mathf.PI;
    }

    void SetUpFromGrapplePoint(PlayerController obj, GameObject externalObject)
    {
        m_TetherPoint = externalObject.transform.position;

        float playerDistance = Vector3.Distance(obj.transform.position, m_TetherPoint);

        playerDistance = Mathf.Clamp(playerDistance, obj.TetherMinLength, obj.TetherMaxLength);

        m_TetherLength = playerDistance;

        m_ReferencePoint = m_TetherPoint + Vector3.down * playerDistance;

        // Angle of player to down - used to maintain momentum
        float playerRightAngle = /*AngleOfPlayerToTether(obj, false)*/AngleOfPlayerToTether(obj, obj.transform.right);

        float playerForwardAngle = /*AngleOfPlayerToTether(obj, true)*/AngleOfPlayerToTether(obj, obj.transform.forward);

        m_ForwardSwingAngle = m_TargetSwingAngle = Mathf.Abs(playerForwardAngle);

        m_RightSwingAngle = Mathf.Abs(playerRightAngle);

        m_StartTime = Time.time;

        m_TrigOffset = (3f / 2) * Mathf.PI;  // so swing starts correctly - not at bottom
    }

    float AngleOfPlayerToTether(PlayerController obj, Vector3 axis)
    {
        axis.y = 1f;

        Vector3 directionToPlayer = (obj.transform.position - m_TetherPoint).normalized;

        directionToPlayer.Scale(axis);

        return Vector3.Angle(directionToPlayer, Vector3.down);
    }

    void HandleLengthAdjustment(PlayerController obj)
    {
        m_AdjustingLength = obj.Input.ActionHeld;

        // Player can move up and down rope
        if (m_AdjustingLength)
        {
            float targetLength = obj.Input.RawMoveInput.z > 0.1f
                ? obj.TetherMinLength
                : (obj.Input.RawMoveInput.z < -0.1f
                    ? obj.TetherMaxLength
                    : m_TetherLength);

            m_TetherLength = Mathf.MoveTowards(m_TetherLength, targetLength, 1.2f * Time.deltaTime);
        }
    }

    void HandleMovement(PlayerController obj)
    {
        m_Time = (obj.SwingSpeed * (Time.time - m_StartTime)) % (2 * Mathf.PI);

        UpdateMaxSwingAngle(obj, m_Time);

        // Handles forward swing motion
        MoveAroundAxis(obj, obj.transform.right, m_ForwardSwingAngle, out m_AngleForward);

        // Handles left, right swing motion
        MoveAroundAxis(obj, obj.transform.forward, m_RightSwingAngle, out m_AngleRight);

        // Don't want to rotate player obj directly, this is a sub object that just rotates the mesh
        Vector3 swingDirection = Quaternion.Euler(-m_AngleForward * obj.transform.right) * obj.transform.forward;
        obj.RotateMeshTo(swingDirection);

        // v = wACos(wt) forward speed
        m_SpeedForward = (m_ForwardSwingAngle * Mathf.Deg2Rad) * Mathf.Cos(m_Time + m_TrigOffset);

        Vector3 velocity = swingDirection * m_SpeedForward * m_TetherLength;
        obj.SetVelocity(velocity);  // Used to maintain momentum when jumping, etc...

        obj.Anim.SetFloat("Forward", m_SpeedForward);

        // Centers the player again
        m_RightSwingAngle = Mathf.Lerp(m_RightSwingAngle, 0f, Time.deltaTime * 2f);

        ResetReferencePoint();
    }

    void MoveAroundAxis(PlayerController obj, Vector3 axis, float maxSwingAngle, out float targetAngle)
    {
        float maxAngleRadians = maxSwingAngle * Mathf.Deg2Rad;

        // x = ASin(wt)
        targetAngle = maxAngleRadians * Mathf.Sin(m_Time + m_TrigOffset) * Mathf.Rad2Deg;

        // Rotate imaginary rope by angle player should now be at
        Vector3 positionFromTether = Quaternion.Euler(-targetAngle * axis) * (m_ReferencePoint - m_TetherPoint).normalized * m_TetherLength;

        m_ReferencePoint = m_TetherPoint + positionFromTether;

        // Setting velocity causes glitches, so move player to target with collisions
        obj.DirectMove(m_ReferencePoint - obj.transform.position);
    }

    void ResetReferencePoint()
    {
        m_ReferencePoint = m_TetherPoint + Vector3.down * m_TetherLength;
    }

    void HandleRotation(PlayerController obj)
    {
        if (m_AdjustingLength || m_IsSwinging)
            return;

        obj.transform.rotation = Quaternion.Euler(0f, obj.transform.eulerAngles.y + obj.Input.RawMoveInput.x, 0f);
    }

    void UpdateMaxSwingAngle(PlayerController obj, float time)
    {
        if (!m_IsSwinging)
        {
            m_TargetSwingAngle = 0f;
        }
        else if (m_CanBoost && time > Mathf.PI)
        {
            m_TargetSwingAngle += obj.SwingAngleBoost;

            m_TargetSwingAngle = Mathf.Clamp(m_TargetSwingAngle, 0f, obj.MaxSwingAngle);

            m_CanBoost = false;
        }
        else if (time >= 0f && time <= Mathf.PI)
        {
            m_CanBoost = true;
        }

        // Lerp stops character jerking up or down
        m_ForwardSwingAngle = Mathf.Lerp(m_ForwardSwingAngle, m_TargetSwingAngle, Time.deltaTime * (m_IsSwinging ? 0.6f : 0.4f));
    }

    void DrawRope(PlayerController obj)
    {
        m_LineRenderer.positionCount = 4;

        m_LineRenderer.SetPosition(0, obj.GrappleHolder.position);
        m_LineRenderer.SetPosition(1, obj.MidLeftHand.position);
        m_LineRenderer.SetPosition(2, obj.MidRightHand.position);
        m_LineRenderer.SetPosition(3, m_TetherPoint);

        m_LineRenderer.startWidth = m_LineRenderer.endWidth = 0.012f;
    }
}
