﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    #region Public Properties

    public Vector3 RawMoveInput { get; private set; }

    public bool WalkHeld { get { return Input.GetButton(m_Walk); } }

    public bool Fire { get { return Input.GetButtonDown(m_Fire); } }

    public bool CombatHeld { get { return Input.GetButton(m_Combat); } }

    public bool Jump { get { return Input.GetButtonDown(m_Jump); } }

    public bool Grapple { get { return Input.GetButtonDown(m_Grapple); } }

    public bool LetGo { get { return Input.GetButtonDown(m_LetGo); } }

    public bool Action { get { return Input.GetButtonDown(m_Action); } }

    public bool ActionHeld { get { return Input.GetButton(m_Action); } }

    #endregion

    #region Serialized Fields

    [SerializeField]
    float m_MoveDeadZone = 0.1f;

    [SerializeField]
    string m_HorizontalAxis = "Horizontal_P1";

    [SerializeField]
    string m_VerticalAxis = "Vertical_P1";

    [SerializeField]
    string m_Walk = "Walk";

    [SerializeField]
    string m_Action = "Action";

    [SerializeField]
    string m_Jump = "Jump_P1";

    [SerializeField]
    string m_LetGo = "LetGo";

    [SerializeField]
    string m_Combat = "Combat";

    [SerializeField]
    string m_Fire = "Fire";

    [SerializeField]
    string m_Grapple = "Jump_P1";

    #endregion

    public void UpdateMoveInput()
    {
        float horizontal = Input.GetAxisRaw(m_HorizontalAxis);
        float vertical = Input.GetAxisRaw(m_VerticalAxis);

        Vector3 input = new Vector3(horizontal, 0f, vertical);

        RawMoveInput = input.magnitude > m_MoveDeadZone ? input : Vector3.zero;
    }

    public Vector3 MoveInputToCamera(Transform camera)
    {
        return Quaternion.Euler(0f, camera.eulerAngles.y, 0f) * RawMoveInput;
    }
}
