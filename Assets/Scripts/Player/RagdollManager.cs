﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{
    [SerializeField]
    Rigidbody[] m_Rigidbodies;

    void Start()
    {
        DisableAll();
    }

    void OnEnable()
    {
        DeathTrigger.OnPlayerDeath += PlayerDeath;
        GetComponent<Health>().OnUnitDead += PlayerDeath;
    }

    void OnDisable()
    {
        DeathTrigger.OnPlayerDeath -= PlayerDeath;
        GetComponent<Health>().OnUnitDead -= PlayerDeath;
    }

    void PlayerDeath()
    {
        EnableAll();
    }

    void DisableAll()
    {
        foreach(Rigidbody rb in m_Rigidbodies)
        {
            Collider col = rb.GetComponent<Collider>();

            col.enabled = false;
            rb.isKinematic = true;
            rb.useGravity = false;
        }
    }

    void EnableAll()
    {
        foreach (Rigidbody rb in m_Rigidbodies)
        {
            Collider col = rb.GetComponent<Collider>();

            col.enabled = true;
            rb.isKinematic = false;
            rb.useGravity = true;
        }
    }
}
