﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public delegate void DeadEvent();
    public delegate void HealthChangeEvent(int amount);

    public event DeadEvent OnUnitDead;
    public event HealthChangeEvent OnHealthChanged;

    [SerializeField]
    int m_MaxHealth = 100;

    // The total health of this unit
    [SerializeField]
    int m_Health = 100;

    public void DoDamage(int damage)
    {
        m_Health -= damage;

        if(m_Health <= 0)
        {
            OnUnitDead.Invoke();
        }

        OnHealthChanged.Invoke(m_Health);
    }

    public void Heal(int amount)
    {
        m_Health += amount;

        if (m_Health > m_MaxHealth)
            m_Health = m_MaxHealth;

        OnHealthChanged.Invoke(m_Health);
    }

    public void RestoreAll()
    {
        m_Health = m_MaxHealth;

        OnHealthChanged.Invoke(m_Health);
    }

    public bool IsAlive()
    {
        return m_Health > 0;
    }

    public int Amount
    {
        get { return m_Health; }
    }
}
