﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Idle contains the player detection code
public class Patrol : Idle
{
    public override void OnEnter(AIController obj)
    {
        Vector3 firstPoint = obj.Waypoints.NextPoint().position;

        obj.NavAgent.speed = obj.WalkSpeed;
        obj.NavAgent.stoppingDistance = 0f;
        obj.NavAgent.SetDestination(firstPoint);
    }

    public override void Update(AIController obj)
    {
        base.Update(obj);

        if (obj.NavAgent.remainingDistance < 1f)
        {
            Vector3 nextPoint = obj.Waypoints.NextPoint().position;

            obj.NavAgent.SetDestination(nextPoint);
        }
    }
}
