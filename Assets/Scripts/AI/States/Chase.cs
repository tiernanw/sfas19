﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : StateBase<AIController>
{
    public override void OnEnter(AIController obj)
    {
        obj.NavAgent.speed = obj.RunSpeed;
    }

    public override void OnExit(AIController obj)
    {
        // Resets stopping distance - attack can make it lower
        obj.NavAgent.stoppingDistance = obj.StoppingDistance;
    }

    public override void Update(AIController obj)
    {
        obj.NavAgent.SetDestination(obj.PlayerTransform.position);

        // Stops enemy coming too close if player is visible again
        obj.NavAgent.stoppingDistance = obj.CheckIfSeePlayer() 
                        ? obj.StoppingDistance 
                        : obj.BlindStoppingDistance;


        if (obj.Distance <= obj.NavAgent.stoppingDistance)
        {
            obj.StateMachine.GoToState("attack");
            return;
        }
    }
}
