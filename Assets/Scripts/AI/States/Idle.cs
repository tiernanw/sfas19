﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : StateBase<AIController>
{
    float m_RequiredDot = 0f;

    public override void OnEnter(AIController obj)
    {
        // We can assume |a||b| will be 1
        m_RequiredDot = Mathf.Cos(obj.FieldOfView * Mathf.Deg2Rad);
    }

    public override void Update(AIController obj)
    {
        if (obj.Distance < obj.AggroRange && obj.CheckIfSeePlayer())
        {
            // Direction to instead of player direction to check if player is behind or infront
            Vector3 dirToPlayer = (obj.PlayerTransform.position - obj.transform.position).normalized;

            // Unity's Vector3.Angle is constrained to 180 degrees, so dont know if behind or not
            float dotProduct = Vector3.Dot(obj.transform.forward, dirToPlayer);

            Debug.Log("dot: " + dotProduct);

            // Player is in front of bad guy
            if (dotProduct > m_RequiredDot)
            {
                obj.StateMachine.GoToState("chase");
                return;
            }
        }
    }
}
