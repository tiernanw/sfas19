﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : StateBase<AIController>
{
    public override void OnEnter(AIController obj)
    {
        obj.Anim.SetBool("isAttacking", true);
    }

    public override void OnExit(AIController obj)
    {
        obj.Anim.SetBool("isAttacking", false);
    }

    public override void Update(AIController obj)
    {
        AnimatorStateInfo animState1 = obj.Anim.GetCurrentAnimatorStateInfo(1);

        if (!obj.CheckIfSeePlayer())
        {
            // Too close to run any closer
            if (obj.Distance < 1f)
                return;

            obj.NavAgent.stoppingDistance = obj.BlindStoppingDistance;
            obj.StateMachine.GoToState("chase");
            return;
        }
        else if (obj.Distance > obj.FireRange)
        {
            obj.StateMachine.GoToState("chase");
            return;
        }

        obj.LookAtFlat(obj.PlayerTransform.position);

        obj.TestForMelee();

        if (animState1.IsName("Aim"))
            obj.Weapon.Fire(obj.PlayerTransform.position + Vector3.up * 1.4f);
    }

}
