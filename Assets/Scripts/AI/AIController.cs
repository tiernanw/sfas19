﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIController : MonoBehaviour
{
    #region Public Properties

    public float RunSpeed
    {
        get { return m_RunSpeed; }
    }

    public float WalkSpeed
    {
        get { return m_WalkSpeed; }
    }

    public float FieldOfView
    {
        get { return m_FieldOfView; }
    }

    public float AggroRange
    {
        get { return m_AggroRange; }
    }

    public float MeleeRange
    {
        get { return m_MeleeRange; }
    }

    public float FireRange
    {
        get { return m_FireRange; }
    }

    public float StoppingDistance
    {
        get { return m_StoppingDistance; }
    }

    public float BlindStoppingDistance
    {
        get { return m_BlindStoppingDistance; }
    }

    public float Distance
    {
        get { return m_Distance; }
    }

    public Animator Anim
    {
        get { return m_Anim; }
    }

    public StateMachine<AIController> StateMachine
    {
        get { return m_StateMachine; }
    }

    public PlayerController PlayerControl
    {
        get { return m_PlayerController; }
    }

    public Transform PlayerTransform
    {
        get { return m_PlayerTransform; }
    }

    public NavMeshAgent NavAgent
    {
        get { return m_NavAgent; }
    }

    public WaypointSet Waypoints
    {
        get { return m_PatrolWaypoints; }
    }

    public GunLogic Weapon
    {
        get { return m_Weapon; }
    }

    #endregion

    #region Serialized Fields

    [Header("AI Properties")]

    // Type of AI behaviour
    [SerializeField]
    AIType m_AIBehaviour = AIType.Idle;

    // Only used if patrol type - points to patrol
    [SerializeField]
    WaypointSet m_PatrolWaypoints;

    // Angle AI can see on either side of eyes
    [SerializeField]
    float m_FieldOfView = 80f;

    // Range in which player can be detected
    [SerializeField]
    float m_AggroRange = 15.0f;

    [Header("Movement Settings")]

    // The AI's running speed
    [SerializeField]
    float m_RunSpeed = 4.0f;

    // The AI's walk speed - normally patrolling
    [SerializeField]
    float m_WalkSpeed = 1.6f;

    // Stopping distance when can't see player
    [SerializeField]
    float m_BlindStoppingDistance = 1f;

    [Header("Attack Settings")]

    // Range at which player can be punched
    [SerializeField]
    float m_MeleeRange = 2.0f;

    // Range at which enemy can shoot
    [SerializeField]
    float m_FireRange = 8.0f;

    // Minimum time between punches
    [SerializeField]
    float m_PunchCoolOff = 2.0f;

    // Current weapon AI has
    [SerializeField]
    GunLogic m_Weapon;

    #endregion

    #region Private Fields

    // AI's animator
    Animator m_Anim;

    // AI's NavMeshAgent used for moving around
    NavMeshAgent m_NavAgent;

    // State machine used to control AI behaviour
    StateMachine<AIController> m_StateMachine;

    // Whether the player is alive or not
    bool m_IsAlive = true;

    // Time at which the AI last melee'd the player
    float m_LastPunchTime = 0f;

    // Distance from player
    float m_Distance = 0f;

    // Distance to stop in front of player normally
    float m_StoppingDistance = 0f;

    PlayerController m_PlayerController;

    Transform m_PlayerTransform;

    #endregion

    #region Private Methods

    void Start()
    {
        m_Anim = GetComponent<Animator>();
        m_NavAgent = GetComponent<NavMeshAgent>();

        m_StoppingDistance = m_NavAgent.stoppingDistance;

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player)
        {
            m_PlayerController = player.GetComponent<PlayerController>();
            m_PlayerTransform = player.transform;
        }

        SetUpStateMachine();
    }

    void OnEnable()
    {
        GetComponent<Health>().OnUnitDead += Die;
    }

    void OnDisable()
    {
        GetComponent<Health>().OnUnitDead -= Die;
    }

    void SetUpStateMachine()
    {
        m_StateMachine = new StateMachine<AIController>(this);

        m_StateMachine.AddState("idle", new Idle());
        m_StateMachine.AddState("patrol", new Patrol());
        m_StateMachine.AddState("chase", new Chase());
        m_StateMachine.AddState("attack", new Attack());

        string behaviour;
        switch (m_AIBehaviour)
        {
            case AIType.Patrol:
                behaviour = "patrol";
                break;
            case AIType.Aggro:
                behaviour = "attack";
                break;
            default:
                behaviour = "idle";
                break;
        }

        m_StateMachine.GoToState(behaviour);
    }

    void Update()
    {
        if (!m_IsAlive)
        {
            return;
        }

        m_Distance = Vector3.Distance(m_PlayerTransform.position, transform.position);

        m_StateMachine.Update();

        m_Anim.SetFloat("Speed", m_NavAgent.velocity.magnitude);
    }

    #endregion

    #region Public Methods

    public void LookAtFlat(Vector3 point)
    {
        Vector3 direction = point - transform.position;
        direction.y = 0f;
        direction.Normalize();

        transform.rotation = Quaternion.LookRotation(direction);
    }

    public void TestForMelee()
    {
        if (Time.time - m_LastPunchTime > m_PunchCoolOff && m_Distance < m_MeleeRange)
        {
            m_Anim.SetTrigger("Punch");
            m_LastPunchTime = Time.time;
        }
    }

    public bool CheckIfSeePlayer()
    {
        Transform headBone = m_Anim.GetBoneTransform(HumanBodyBones.Head);

        Vector3 dirToPlayer = (m_PlayerTransform.position + Vector3.up * 1.4f) - headBone.position;

        RaycastHit hit;
        if (Physics.Raycast(headBone.position, dirToPlayer.normalized, out hit, dirToPlayer.magnitude, ~(1 << 9)))
        {
            if (!hit.collider.CompareTag("Player"))
                return false;
        }

        return true;
    }

    public void PunchPlayer()
    {
        m_PlayerController.AddForce((transform.forward + new Vector3(0, 2, 0)) * 20.0f);
    }

    public void Die()
    {
        m_IsAlive = false;

        Destroy(gameObject);
    }

    #endregion
}

public enum AIType
{
    Idle,
    Patrol,
    Aggro
}
