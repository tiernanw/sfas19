﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunLogic : MonoBehaviour
{
    // The Bullet Prefab
    [SerializeField]
    GameObject m_BulletPrefab;

    // The Explosive Bullet Prefab
    [SerializeField]
    GameObject m_ExplosiveBulletPrefab;

    // The Bullet Spawn Point
    [SerializeField]
    Transform m_BulletSpawnPoint;

    // The Bullet Spawn Point
    [SerializeField]
    float m_ShotCooldown = 0.5f;

    bool m_CanShoot = true;

    // Current cool down countdown
    float m_CountDown = 0f;

    // VFX
    [SerializeField]
    ParticleSystem m_Flare;

    [SerializeField]
    ParticleSystem m_Smoke;

    [SerializeField]
    ParticleSystem m_Sparks;

    // SFX
    [SerializeField]
    AudioClip m_BulletShot;

    [SerializeField]
    AudioClip m_GrenadeLaunched;

    // The AudioSource to play Sounds for this object
    AudioSource m_AudioSource;

    [SerializeField]
    int m_BulletAmmo = 100;

    [SerializeField]
    int m_GrenadeAmmo = 5;

    UIManager m_UIManager;

    void Start ()
    {
        m_AudioSource = GetComponent<AudioSource>();
        m_UIManager = FindObjectOfType<UIManager>();

        // Update UI
        if (m_UIManager)
        {
            m_UIManager.SetAmmoText(m_BulletAmmo, m_GrenadeAmmo);
        }
    }
	
	void Update ()
    {
        if (!m_CanShoot)
        {
            m_CountDown -= Time.deltaTime;
            if (m_CountDown < 0.0f)
            {
                m_CanShoot = true;
                m_CountDown = m_ShotCooldown;
            }
        }

       /* if (m_CanShoot)
        {
            if (Input.GetButtonDown("Fire1") && m_BulletAmmo > 0)
            {
                Fire();
                m_CanShoot = false;
            }
            else if (Input.GetButtonDown("Fire2") && m_GrenadeAmmo > 0)
            {
                FireGrenade();
                m_CanShoot = false;
            }
        }*/
    }

    public void Fire(Vector3 atPoint)
    {
        if (!m_CanShoot)
            return;

        if (m_BulletAmmo <= 0)
        {
            if (m_Smoke)
                m_Smoke.Play();

            return;
        }

        if(m_BulletPrefab)
        {
            // Reduce the Ammo count
            --m_BulletAmmo;

            // Find direction & then rotation
            Vector3 direction = (atPoint - m_BulletSpawnPoint.position).normalized;
            Quaternion bulletRotation = Quaternion.LookRotation(direction);

            // Create the Projectile from the Bullet Prefab
            Instantiate(m_BulletPrefab, m_BulletSpawnPoint.position, bulletRotation * m_BulletPrefab.transform.rotation);
        
            // Play Particle Effects
            PlayGunVFX();

            // Play Sound effect
            if(m_AudioSource && m_BulletShot)
            {
                m_AudioSource.PlayOneShot(m_BulletShot);
            }

            // Update UI
            if(m_UIManager)
            {
                m_UIManager.SetAmmoText(m_BulletAmmo, m_GrenadeAmmo);
            }
        }

        m_CanShoot = false;
    }

    public void FireGrenade(Vector3 atPoint)
    {
        if (!m_CanShoot)
            return;

        if (m_GrenadeAmmo <= 0)
        {
            if (m_Smoke)
                m_Smoke.Play();

            return;
        }

        if (m_ExplosiveBulletPrefab)
        {
            // Reduce the Ammo count
            --m_GrenadeAmmo;

            // Find direction & then rotation
            Vector3 direction = (atPoint - m_BulletSpawnPoint.position).normalized;
            Quaternion bulletRotation = Quaternion.LookRotation(direction);

            // Create the Projectile from the Explosive Bullet Prefab
            Instantiate(m_ExplosiveBulletPrefab, m_BulletSpawnPoint.position, bulletRotation * m_ExplosiveBulletPrefab.transform.rotation);

            // Play Particle Effects
            PlayGunVFX();

            // Play Sound effect
            if (m_AudioSource && m_GrenadeLaunched)
            {
                m_AudioSource.PlayOneShot(m_GrenadeLaunched);
            }

            // Update UI
            if(m_UIManager)
            {
                m_UIManager.SetAmmoText(m_BulletAmmo, m_GrenadeAmmo);
            }
        }
    }

    void PlayGunVFX()
    {
        if (m_Flare)
        {
            m_Flare.Play();
        }

        if (m_Sparks)
        {
            m_Sparks.Play();
        }

        if (m_Smoke)
        {
            m_Smoke.Play();
        }
    }

    public void AddAmmo(int bullets, int grenades)
    {
        m_BulletAmmo += bullets;
        m_GrenadeAmmo += grenades;

        // Update UI
        if (m_UIManager)
        {
            m_UIManager.SetAmmoText(m_BulletAmmo, m_GrenadeAmmo);
        }
    }
}
