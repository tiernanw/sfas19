﻿
public abstract class StateBase<T>
{
    public virtual void OnEnter(T obj) { }
    public virtual void OnExit(T obj) { }
    public virtual void LateUpdate(T obj) { }
    public virtual void HandleMessage(T obj, object o) { }

    public abstract void Update(T obj);
}