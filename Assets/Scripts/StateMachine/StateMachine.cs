﻿using System;
using System.Collections;
using System.Collections.Generic;

public class StateMachine<T>
{
    T m_Owner;

    StateBase<T> m_State;

    string m_CurrentState;

    // Dictionary provides O(1) look up times - avoids GC as well
    Dictionary<string, StateBase<T>> m_AllStates;

    public StateMachine(T owner)
    {
        m_Owner = owner;
        m_AllStates = new Dictionary<string, StateBase<T>>();
    }

    public void Update()
    {
        m_State.Update(m_Owner);
    }

    public void LateUpdate()
    {
        m_State.LateUpdate(m_Owner);
    }

    public void AddState(string key, StateBase<T> state)
    {
        m_AllStates.Add(key, state);
    }

    public void GoToState(string name)
    {
        StateBase<T> newState;

        if (m_AllStates.TryGetValue(name, out newState))
        {
            m_CurrentState = name;

            if (m_State != null)
                m_State.OnExit(m_Owner);
            m_State = newState;
            m_State.OnEnter(m_Owner);
        }
        else
        {
            throw new StateNotAvailable(name, "State key '" + name + "' did not return a state in the machine.");
        }
    }

    public void SendMessage(object o)
    {
        m_State.HandleMessage(m_Owner, o);
    }

    public string CurrentState
    {
        get { return m_CurrentState; }
    }
}

public class StateNotAvailable : Exception
{
    string m_Name;

    public StateNotAvailable(string name)
    {
        m_Name = name;
    }

    public StateNotAvailable(string name, string message)
        : base(message)
    {
        m_Name = name;
    }

    public string Name
    {
        get { return m_Name; }
    }
}
