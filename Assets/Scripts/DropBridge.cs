﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropBridge : MonoBehaviour
{
    Rigidbody m_BridgeRB;

    void Start()
    {
        m_BridgeRB = GetComponent<Rigidbody>();

        m_BridgeRB.isKinematic = false;
        m_BridgeRB.useGravity = true;
    }
}
