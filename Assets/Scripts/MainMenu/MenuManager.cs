﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    GameObject m_Logo;

    [SerializeField]
    GameObject m_MainPanel;

    [SerializeField]
    GameObject m_SettingsPanel;

    // What panel are we currently viewing
    PanelType m_CurrentPanel = PanelType.Main;

    void Update()
    {
        if (Input.GetButton("Cancel"))
        {
            switch(m_CurrentPanel)
            {
                case PanelType.Settings:
                    ShowMainPanel();
                    break;
                default:
                    break;
            }
        }
    }

    public void ShowSettings()
    {
        HideAll();

        m_SettingsPanel.SetActive(true);

        m_CurrentPanel = PanelType.Settings;
    }

    public void ShowMainPanel()
    {
        HideAll();

        m_Logo.SetActive(true);
        m_MainPanel.SetActive(true);

        m_CurrentPanel = PanelType.Main;
    }

    public void NewGame()
    {
        SceneManager.LoadScene(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    void HideAll()
    {
        m_Logo.SetActive(false);
        m_MainPanel.SetActive(false);
        m_SettingsPanel.SetActive(false);
    }

    enum PanelType
    {
        Main,
        Settings
    }
}
