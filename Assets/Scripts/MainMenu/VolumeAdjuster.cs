﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeAdjuster : MonoBehaviour
{
    [SerializeField]
    Slider m_MusicSlider;

    [SerializeField]
    Slider m_SFXSlider;

    [SerializeField]
    AudioMixer m_AudioMixer;

    public void MusicVolumeChanged(float value)
    {
        float decibel = ConvertToDecibel(value / m_MusicSlider.maxValue);

        m_AudioMixer.SetFloat("MusicVol", decibel);
    }

    public void SFXVolumeChanged(float value)
    {
        float decibel = ConvertToDecibel(value / m_SFXSlider.maxValue);

        m_AudioMixer.SetFloat("SFXVol", decibel);
    }

    float ConvertToDecibel(float value)
    {
        return Mathf.Log10(Mathf.Max(value, 0.0001f)) * 20f;
    }
}
